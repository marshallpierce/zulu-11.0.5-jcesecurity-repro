# alpine:3.9
FROM alpine@sha256:7746df395af22f04212cd25a92c1d6dbc5a06a0ca9579a229ef43008d4d1302a

ARG JDK_VERSION=zulu11.35.13-ca-jdk11.0.5

RUN mkdir -p /usr/local/zulu \
    && wget https://cdn.azul.com/zulu/bin/${JDK_VERSION}-linux_musl_x64.tar.gz -qO - \
    | tar -xz -f - -C /usr/local/zulu

ENV JAVA_HOME=/usr/local/zulu/${JDK_VERSION}-linux_musl_x64
ENV PATH $PATH:$JAVA_HOME/bin

RUN mkdir -p /repro/tmp
WORKDIR /repro
COPY /build/libs/zulu-11.0.5-JceSecurity-repro.jar /repro/repro.jar

CMD java -cp repro.jar \
    -Xmx200m \
    -XX:+HeapDumpOnOutOfMemoryError \
    -XX:HeapDumpPath=tmp/dump-$(date +%Y-%m-%d-%H-%m-%S).hprof \
    org.mpierce.jvm.JceSecurityRepro
