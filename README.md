Reproduction of memory leak in JDK 11.0.5's JceSecurity.

As of Zulu 11.0.5, `JceSecurity.IdentityWrapper` appears to be trying to cache verification results, but it is calculating the identity hash code on the `WeakReference` wrapper, which is different every time, so the cache lookup will never succeed. This means that every time anything happens that's crypto related all the crypto providers end up being re-cached. 

This repo simply loops creating a `Mac` so that you can capture a heap dump when it inevitably runs out of memory.

Reproduced on Zulu 11.0.5. Doesn't repro on AdoptOpenJDK or java.net's 11.0.5 builds, which seem to have the same `JceSecurity` class as in 11.0.4.

- run `./gradlew build`
- run `docker-compose build && docker-compose run repro`, wait a couple seconds, and look for the heap dump to show up in `./tmp`
- Or, to do it by hand, change the current JDK to be Zulu 11.0.5 and run 
    ```
    java -cp build/libs/zulu-11.0.5-JceSecurity-repro.jar \
    -Xmx200m \
    -XX:+HeapDumpOnOutOfMemoryError \
    -XX:HeapDumpPath=tmp/dump.hprof \
    org.mpierce.jvm.JceSecurityRepro
    ``` 
- open resulting heap in analysis tool of choice, e.g. VisualVM, jhat, etc, and see that the `verificationResults` static field in `JceSecurity` is rather large
